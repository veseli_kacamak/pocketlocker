//
//  DTOCard.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class DTOCard: NSObject {
    var cardId: String!
    var cardDescription: String!
    var name: String!
    var pin: String!

    required init(_ cardId: String, description: String, name: String, pin: String) {
        self.cardId = cardId
        cardDescription = description
        self.name = name
        self.pin = pin
    }

    public required init(fromDictionary dictionary: NSDictionary) {
        cardDescription = dictionary["description"] as? String
        name = dictionary["name"] as? String
        pin = dictionary["pin"] as? String
    }
}
