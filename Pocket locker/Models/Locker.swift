//
//  Locker.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class Locker: NSObject, InitializableJSON, NSCoding {
    var cards: [String]!
    var passwords: [String]!

    required init(_ cards: [String], passwords: [String]) {
        self.cards = cards
        self.passwords = passwords
    }

    public required init(fromDictionary dictionary: NSDictionary) {
        cards = [String]()
        if let cardsArray = dictionary["cards"] as? [String] {
            for dic in cardsArray {
                let value = dic
                cards.append(value)
            }
        }
        passwords = [String]()
        if let passwordsArray = dictionary["passwords"] as? [String] {
            for dic in passwordsArray {
                let value = dic
                passwords.append(value)
            }
        }
    }

    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if cards != nil {
            dictionary["cards"] = cards
        }
        if passwords != nil {
            dictionary["passwords"] = passwords
        }
        return dictionary
    }

    @objc public required init(coder _: NSCoder) {
    }

    @objc open func encode(with _: NSCoder) {
    }
}

public protocol InitializableJSON {
    init(fromDictionary dictionary: NSDictionary)
}
