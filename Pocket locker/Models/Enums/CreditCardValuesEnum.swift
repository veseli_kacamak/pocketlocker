//
//  CreditCardValuesEnum.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

enum CreditCardValuesEnum {
    case CreditCardName, CreditCardPin
}
