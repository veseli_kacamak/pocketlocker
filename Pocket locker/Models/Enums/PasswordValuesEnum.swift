//
//  PasswordValuesEnum.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/3/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

enum PasswordValuesEnum {
    case PasswordName, PasswordEmail, Password
}
