//
//  DTOPassword.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class DTOPassword: NSObject {
    var passwordId: String!
    var name: String!
    var password: String!
    var desc: String!
    var eMail: String!

    required init(_ passwordId: String, description: String, name: String, password: String, eMail: String) {
        self.passwordId = passwordId
        desc = description
        self.name = name
        self.password = password
        self.eMail = eMail
    }

    public required init(fromDictionary dictionary: NSDictionary) {
        // passwordId = dictionary["passwordId"] as? String
        desc = dictionary["description"] as? String
        name = dictionary["name"] as? String
        password = dictionary["password"] as? String
        eMail = dictionary["eMail"] as? String
    }
}
