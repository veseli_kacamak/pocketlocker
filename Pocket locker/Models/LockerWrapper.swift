//
//  LockerWrapper.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/21/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class LockerWrapper: NSObject {
    var locker: Locker!
    var passwords: [DTOPassword] = [DTOPassword]()
    var creditCards: [DTOCard] = [DTOCard]()

    required init(_ locker: Locker) {
        self.locker = locker
    }
}
