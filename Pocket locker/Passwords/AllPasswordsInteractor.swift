//
//  AllPasswordsInteractor.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol AllPasswordsInteractor {
    init(_ api: AllPasswordsAPI)
    func getUserLocker(_ userId: String, completion: @escaping (_ result: Locker?, _ error: LockerError?) -> Void)
    func getPassword(_ passwordId: String, completion: @escaping (_ result: DTOPassword?, _ error: LockerError?) -> Void)
    func getCreditCard(_ creditCardId: String, completion: @escaping (_ result: DTOCard?, _ error: LockerError?) -> Void)
    func deletePassword(_ passwordId: String, userPasswords: [String], completion: @escaping (Bool) -> Void)
    func deleteCreditCard(_ creditCardId: String, userCards: [String], completion: @escaping (Bool) -> Void)
}
