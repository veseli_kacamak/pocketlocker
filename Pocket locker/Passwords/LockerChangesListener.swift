//
//  LockerChangesListener.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/15/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol LockerChangesListener: class {
    func savedCard()
    func updatedCard()
    func deletedCard()
    func savedPassword()
    func updatedPassword()
    func deletedPassword()
}
