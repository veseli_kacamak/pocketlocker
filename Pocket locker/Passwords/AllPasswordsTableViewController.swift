//
//  AllPasswordsTableViewController.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/6/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Firebase
import FirebaseAuth
import Foundation
import GoogleSignIn
import UIKit

class AllPasswordsTableViewController: UIViewController, AllPasswordsView, LockerChangesListener {
    @IBOutlet var tableView: UITableView!
    let RIGHT_PADDING_CONSTRAINT: CGFloat = 10.0
    let ROW_HEIGHT: CGFloat = 65.0
    var presenter: AllPasswordsPresenter?
    var numbersOfSections = 2 // 1st section Passwords 2nd Credit Cards
    let ERROR_DIALOG_TITLE = "Error"
    let PASSWORDS_TITLE = "Passwords"
    let CREDIT_CARDS_TITLE = "Credit Cards"
    // cell ids
    let PASSWORD_CELL_ID = "PasswordCell"
    let CREDIT_CARD_CELL_ID = "CreditCardCell"
    // view controllers
    let CREATE_PASSWORD_CONTROLLER = "CreatePasswordViewController"
    let CREATE_CREDIT_CARD_CONTROLLER = "NewCreditCardViewController"
    let SIGN_IN_CONTROLLER = "SignInViewController"
    let MAIN_STORYBOARD = "Main"
    let LOCKER_TITTLE = "Locker"
    let EMPTY_MESSAGE = "Locker is empty"
    var searchBar:UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setupUI()
        setupPresenter()
    }

    /* setting up presenter */
    func setupPresenter() {
        InjectorHelper.inject(self, lockerListener: self)
        presenter?.getUserLocker()
    }

    /* setting up UI */
    fileprivate func setupNavigationController() {
        self.title = LOCKER_TITTLE
        searchBar = UISearchController(searchResultsController: nil)
        searchBar?.searchResultsUpdater = self
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.searchController = searchBar
        } else {
        }
    }
    
    func setupUI() {
        /* setting delegate */
        tableView.delegate = self
        tableView.dataSource = self
        /* setting table view cell */
        tableView.register(UINib(nibName: PASSWORD_CELL_ID, bundle: nil), forCellReuseIdentifier: PASSWORD_CELL_ID)
        tableView.register(UINib(nibName: CREDIT_CARD_CELL_ID, bundle: nil), forCellReuseIdentifier: CREDIT_CARD_CELL_ID)
        tableView.estimatedRowHeight = ROW_HEIGHT

        /* pull to refresh */
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
    }

    /* reload table when showing all credit cards and passowrds */
    func showAllPasswordsAndCreditCards() {
        tableView.reloadData()
    }

    /* refresh credit card section */
    func refreshCreditCardsSection() {
        tableView.reloadData()
    }

    /* refresh password section */
    func refreshPasswordsSection() {
        tableView.reloadData()
    }

    /* show error dialog */
    func showError(_ errorMessage: String) {
        Utils.sharedInstance.createAlert(self, title: ERROR_DIALOG_TITLE, message: errorMessage)
    }

    func showEmptyMessage() {
        self.title = EMPTY_MESSAGE
        Utils.sharedInstance.createAlert(self, title: ERROR_DIALOG_TITLE, message: "No data available!")
    }
    
    func hideEmptyMessage() {
        self.title = LOCKER_TITTLE
    }

    /* reseting search bar  */
    func resetSearcBar() {
    }

    /* pull to refresh action */
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        presenter?.resetSearchBar()
        presenter?.getUserLocker()
        refreshControl.endRefreshing()
    }

    /* logout user */
    func logoutUser(_ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }

    /* successfull logout */
    func successLogout() {
        let viewController: UIViewController = UIStoryboard(name: MAIN_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SIGN_IN_CONTROLLER) as UIViewController
        present(viewController, animated: false, completion: nil)
    }

    /* showing delete alert */
    func showDeleteAlert(_ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }

    /* credit card successfully saved */
    func savedCard() {
        presenter?.getUserLocker()
    }

    /* credit card successfully updated */
    func updatedCard() {
        presenter?.getUserLocker()
    }

    /* credit card successfully deleted */
    func deletedCard() {
        presenter?.getUserLocker()
    }

    /* password successfully saved */
    func savedPassword() {
        presenter?.getUserLocker()
    }

    /* password successfully updated */
    func updatedPassword() {
        presenter?.getUserLocker()
    }

    /* password successfully deleted */
    func deletedPassword() {
        presenter?.getUserLocker()
    }
    
    /* edit password */
    func editPassword(_ passwordId: String, selectedPassword: DTOPassword, userPasswordIds: [String]) {
        let viewController: CreatePasswordViewController = UIStoryboard(name: MAIN_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CREATE_PASSWORD_CONTROLLER) as! CreatePasswordViewController
        viewController.userPasswords =  userPasswordIds
        viewController.nameValue = selectedPassword.name
        viewController.emailValue = selectedPassword.eMail
        viewController.passwordValue =  selectedPassword.password
        viewController.passwordId = passwordId
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    /* edit credit card */
    func editCreditCard(_ creditCardId: String, selectedCreditCard: DTOCard, userCreditCardIds: [String]) {
        let viewController: NewCreditCardViewController = UIStoryboard(name: MAIN_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CREATE_CREDIT_CARD_CONTROLLER) as! NewCreditCardViewController
        viewController.userCreditCards = userCreditCardIds
        viewController.nameValue = selectedCreditCard.name
        viewController.pinValue = selectedCreditCard.pin
        viewController.creditCardId = creditCardId
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func newPassword() {
        let viewController: CreatePasswordViewController = UIStoryboard(name: MAIN_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CREATE_PASSWORD_CONTROLLER) as! CreatePasswordViewController
        viewController.userPasswords = (presenter?.getUserPasswordIds())!
        viewController.lockerChangesListener = self
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func newCreditCard() {
        let viewController: NewCreditCardViewController = UIStoryboard(name: MAIN_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CREATE_CREDIT_CARD_CONTROLLER) as! NewCreditCardViewController
        viewController.userCreditCards = (presenter?.getUserCreditCardIds())!
        viewController.lockerChangesListener = self
        navigationController?.pushViewController(viewController, animated: true)
    }

    /* add new credit card action */
    @IBAction func AddNewCreditCardAction(_: Any) {
        presenter?.newCreditCard()
    }

    /* add new password action */
    @IBAction func AddNewPasswordAction(_: Any) {
        presenter?.newPassword()
    }

    /* logout action */
    @IBAction func LogoutAction(_: Any) {
        presenter?.logoutUser()
    }
}

/* search bar extension for controller */
extension AllPasswordsTableViewController: UISearchResultsUpdating {
    
    /* filter search */
    func updateSearchResults(for searchController: UISearchController) {
        if !searchController.isBeingDismissed {
            presenter?.search(searchController.searchBar.text!)
        }
    }
}

/* table extension for view controller */
extension AllPasswordsTableViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? PASSWORDS_TITLE : CREDIT_CARDS_TITLE
    }

    func numberOfSections(in _: UITableView) -> Int {
        return numbersOfSections
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? (presenter?.getLockerPasswords().count)! : (presenter?.getCreditCards().count)!
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            presenter?.openPassword(indexPath.row)
        } else {
            presenter?.openCreditCard(indexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let password = presenter?.getLockerPasswords()[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: PASSWORD_CELL_ID, for: indexPath) as! PasswordCell
            cell.setupCell((password?.name)!, password: (password?.password)!)
            return cell
        } else {
            let creditCard = presenter?.getCreditCards()[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CREDIT_CARD_CELL_ID, for: indexPath) as! CreditCardCell
            cell.setupCell((creditCard?.name)!, pin: (creditCard?.pin)!)
            return cell
        }
    }

    func tableView(_: UITableView, canEditRowAt _: IndexPath) -> Bool {
        return true
    }

    func tableView(_: UITableView, editingStyleForRowAt _: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }

    func tableView(_: UITableView, commit _: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            presenter?.deletePasswordAlert((presenter?.getUserPasswordIds()[indexPath.row])!)
        } else {
            presenter?.deleteCreditCardAlert((presenter?.getUserCreditCardIds()[indexPath.row])!)
        }
    }
}
