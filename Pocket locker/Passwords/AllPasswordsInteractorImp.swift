//
//  AllPasswordsInteractorImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class AllPasswordsInteractorImp: AllPasswordsInteractor {
    var api: AllPasswordsAPI

    required init(_ api: AllPasswordsAPI) {
        self.api = api
    }

    /* get user locker for userId */
    func getUserLocker(_ userId: String, completion: @escaping (_ result: Locker?, _ error: LockerError?) -> Void) {
        api.getUserLocker(userId, completition: { locker, error in
            completion(locker, error)
        })
    }

    /* get passord for passwordId */
    func getPassword(_ passwordId: String, completion: @escaping (DTOPassword?, _ error: LockerError?) -> Void) {
        api.getPassword(passwordId, completition: { password, error in
            completion(password, error)
        })
    }

    /* get credit card for creditCardID */
    func getCreditCard(_ creditCardId: String, completion: @escaping (DTOCard?, _ error: LockerError?) -> Void) {
        api.getCreditCard(creditCardId, completition: { creditCard, error in
            completion(creditCard, error)
        })
    }

    /* delete password for passwordId */
    func deletePassword(_ passwordId: String, userPasswords: [String], completion: @escaping (Bool) -> Void) {
        api.deletePassword(passwordId, userPasswords: userPasswords, completition: { isDeleted in
            completion(isDeleted)
        })
    }

    /* delete credit card for creditCardId */
    func deleteCreditCard(_ creditCardId: String, userCards: [String], completion: @escaping (Bool) -> Void) {
        api.deleteCreditCard(creditCardId, userCards: userCards, completition: { isDeleted in
            completion(isDeleted)
        })
    }
}
