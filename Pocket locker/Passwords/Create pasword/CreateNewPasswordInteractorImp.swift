//
//  CreateNewPasswordInteractorImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation

class CreateNewPasswordInteractorImp: CreateNewPasswordInteractor {
    var api: CreateNewPasswordApi

    required init(_ api: CreateNewPasswordApi) {
        self.api = api
    }

    /* save password */
    func savePassword(_ newPassword: DTOPassword, userPasswords: [String], completition: @escaping (_ success: Bool) -> Void) {
        api.savePassword(newPassword, userPasswords: userPasswords, completition: { result in
            completition(result)
        })
    }

    /* update password */
    func updatePassword(_ oldPassword: DTOPassword, completition: @escaping (Bool) -> Void) {
        api.update(oldPassword, completition: { result in
            completition(result)
        })
    }
}
