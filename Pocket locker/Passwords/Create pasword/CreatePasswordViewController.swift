//
//  CreatePasswordViewController.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/29/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

class CreatePasswordViewController: UIViewController, ViewCreateNewPassword {
    @IBOutlet var tableView: UITableView!
    let NEW_PASSWORD_CELL_ID = "NewPasswordCell"
    let TITLE_NAME = "Name"
    let TITLE_EMAIL = "E-mail"
    let TITLE_PASSWORD = "Password"
    var nameValue = ""
    var emailValue = ""
    var passwordValue = ""
    var passwordId = ""
    let ERROR_ALERT_TITLE = "Error"
    let RIGHT_BUTTON_NAME = "Save"
    var userPasswords = [String]()
    var presenter: CreateNewPasswordPresenter?
    var lockerChangesListener: LockerChangesListener?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarButtons()
        setupPresenter()
        setupUI()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    /* setup navigation bar buttons */
    func setupNavigationBarButtons() {
        let save = UIBarButtonItem(title: RIGHT_BUTTON_NAME, style: .plain, target: self, action: #selector(savePasswordAction))
        navigationItem.rightBarButtonItems = [save]
    }

    /* setup presenter */
    func setupPresenter() {
        InjectorHelper.inject(self)
        presenter?.setUserPasswords(userPasswords)
        if nameValue.count > 0 && emailValue.count > 0 && passwordValue.count > 0 && passwordId.count > 0 {
            presenter?.setPasswordUpdating()
            presenter?.setPasswordName(nameValue)
            presenter?.setPasswordEmailPin(emailValue)
            presenter?.setPassword(passwordValue)
            presenter?.setPasswordId(passwordId)
        }
    }

    /* setup UI */
    func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        /* setting table view cell */
        tableView.register(UINib(nibName: NEW_PASSWORD_CELL_ID, bundle: nil), forCellReuseIdentifier: NEW_PASSWORD_CELL_ID)
    }

    /* successfully updated password */
    func successfullyUpdatePassword() {
        lockerChangesListener?.updatedPassword()
        navigationController?.popViewController(animated: false)
    }

    /* successfully saved password */
    func successfullySavedPassword() {
        lockerChangesListener?.savedPassword()
        navigationController?.popViewController(animated: false)
    }

    /* show error alert */
    func error(_ errorMessage: String) {
        Utils.sharedInstance.createAlert(self, title: ERROR_ALERT_TITLE, message: errorMessage)
    }

    /* refresh view */
    func refreshView() {
        tableView.reloadData()
    }

    /* show alert */
    func showAlert(_ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }

    /* save password action */
    @objc func savePasswordAction(_: Any) {
        (presenter?.isPasswordUpdating())! ? presenter?.showUpdatePasswordDialog() : presenter?.savePasswor()
    }

    /* random password action */
    @IBAction func randomPasswordAction(_: Any) {
        presenter?.setRandomPassword()
    }
}

extension CreatePasswordViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NEW_PASSWORD_CELL_ID, for: indexPath) as! NewPasswordCell
        cell.setupPresenter(presenter!)
        switch indexPath.row {
        case 0:
            cell.setHint("example.com")
            cell.setupCell(TITLE_NAME, value: (presenter?.getPasswordName())!, valueType: .PasswordName)
            break
        case 1:
            cell.setHint("name@name.com")
            cell.setupCell(TITLE_EMAIL, value: (presenter?.getPasswordEmail())!, valueType: .PasswordEmail)
            break
        case 2:
            cell.setHint("****")
            cell.setupCell(TITLE_PASSWORD, value: (presenter?.getPassword())!, valueType: .Password)
            break
        default:
            break
        }
        return cell
    }
}
