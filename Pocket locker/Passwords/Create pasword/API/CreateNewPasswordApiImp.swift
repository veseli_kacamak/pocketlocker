//
//  CreateNewPasswordApiImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/14/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import Foundation

class CreateNewPasswordApiImp: CreateNewPasswordApi {
    var networkController: FBDBController

    required init(_ networkController: FBDBController) {
        self.networkController = networkController
        self.networkController.ref.keepSynced(true)
    }

    /* save password */
    func savePassword(_ password: DTOPassword, userPasswords: [String], completition: @escaping (Bool) -> Void) {
        let userId = Auth.auth().currentUser?.uid ?? ""

        let pass = [
            "description": password.desc!,
            "name": password.name!,
            "eMail": password.eMail!,
            "password": password.password!,
        ]

        var passwords = [String: Any]()
        if !userPasswords.isEmpty {
            for i in 0 ... userPasswords.count - 1 {
                passwords["\(i)"] = userPasswords[i]
            }
        }

        // add new value
        let passwordsSize = passwords.count
        let oldPasswordIndex = passwordsSize - 1
        let newPasswordIndex = oldPasswordIndex + 1 < 0 ? 0 : oldPasswordIndex + 1
        passwords["\(newPasswordIndex)"] = "\(password.passwordId!)"

        let childUpdates = [
            "/passwords/\(password.passwordId!)/": pass,
            "/lockers/\(userId)/passwords/": passwords,
        ]
        networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
            completition(true)
        })
    }

    /* update password */
    func update(_ password: DTOPassword, completition: @escaping (Bool) -> Void) {

        let pass = [
            "description": password.desc!,
            "name": password.name!,
            "eMail": password.eMail!,
            "password": password.password!,
        ]

        let childUpdates = ["/passwords/\(password.passwordId!)/": pass]
        networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
            completition(true)
        })
    }
}
