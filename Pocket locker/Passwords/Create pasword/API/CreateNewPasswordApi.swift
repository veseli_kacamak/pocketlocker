//
//  CreateNewPasswordApi.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/14/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol CreateNewPasswordApi {
    init(_ networkController: FBDBController)
    func savePassword(_ password: DTOPassword, userPasswords: [String], completition: @escaping (Bool) -> Void)
    func update(_ password: DTOPassword, completition: @escaping (Bool) -> Void)
}
