//
//  CreateNewPasswordProtocolImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

class CreateNewPasswordPresenterImp: CreateNewPasswordPresenter {
    unowned var view: ViewCreateNewPassword
    var interactor: CreateNewPasswordInteractor
    var userCards = [String]()
    var userPasswords = [String]()
    var passwordName = ""
    var passwordEmail = ""
    var password = ""
    var passwordId = ""
    var updatingPassword = false

    required init(_ view: ViewCreateNewPassword, interactor: CreateNewPasswordInteractor) {
        self.view = view
        self.interactor = interactor
    }

    /* set user passwords */
    func setUserPasswords(_ userPasswords: [String]) {
        self.userPasswords = userPasswords
    }

    /* save password */
    func savePasswor() {
        if passwordName.count > 0 && password.count > 0 && passwordEmail.count > 0 {
            let newPassword = DTOPassword(Utils.sharedInstance.cardAndPasswordIdGenerator(length: 8), description: "description", name: passwordName, password: password, eMail: passwordEmail)
            interactor.savePassword(newPassword, userPasswords: userPasswords, completition: { result in
                result ? self.view.successfullySavedPassword() : self.view.error("failed to save password!")
            })
        } else {
            view.error("You must fill all data!!!")
        }
    }

    /* update password */
    func updatePassword() {
        if passwordName.count > 0 && password.count > 0 && passwordEmail.count > 0 {
            let oldPassword = DTOPassword(passwordId, description: "description", name: passwordName, password: password, eMail: passwordEmail)
            interactor.updatePassword(oldPassword, completition: { result in
                result ? self.view.successfullyUpdatePassword() : self.view.error("failed to update password!")
            })
        } else {
            view.error("You must fill all data!!!")
        }
    }

    /* show update password dialog */
    func showUpdatePasswordDialog() {
        let title = "Update password"
        let message = "Are you sure that you want to update password"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (_: UIAlertAction!) in
        }
        let DeleteAction = UIAlertAction(title: "Update", style: .default) { (_: UIAlertAction!) in
            self.updatePassword()
        }
        alertController.addAction(DeleteAction)
        alertController.addAction(CancelAction)
        view.showAlert(alertController)
    }

    /* set password name */
    func setPasswordName(_ name: String) {
        passwordName = name
    }

    /* set password email */
    func setPasswordEmailPin(_ email: String) {
        passwordEmail = email
    }

    /* set password */
    func setPassword(_ password: String) {
        self.password = password
    }

    /* set password id */
    func setPasswordId(_ id: String) {
        passwordId = id
    }

    /* get password name */
    func getPasswordName() -> String {
        return passwordName
    }

    /* get password email */
    func getPasswordEmail() -> String {
        return passwordEmail
    }

    /* get password */
    func getPassword() -> String {
        return password
    }

    /* set random password with 12 alphanumeric */
    func setRandomPassword() {
        setPassword(Utils.sharedInstance.randomString(length: 12))
        view.refreshView()
    }

    /* set password is updating flag */
    func setPasswordUpdating() {
        updatingPassword = true
    }

    /* check if password is currently updating */
    func isPasswordUpdating() -> Bool {
        return updatingPassword
    }
}
