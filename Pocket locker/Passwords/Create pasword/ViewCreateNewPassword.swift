//
//  ViewCreateNewPassword.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

protocol ViewCreateNewPassword: class {
    func successfullySavedPassword()
    func successfullyUpdatePassword()
    func error(_ errorMessage: String)
    func showAlert(_ alert: UIAlertController)
    func refreshView()
}
