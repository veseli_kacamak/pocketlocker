//
//  NewPasswordCell.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/29/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

class NewPasswordCell: UITableViewCell {
    @IBOutlet var tfValue: UITextField!
    @IBOutlet var lblTitle: UILabel!
    var valueType: PasswordValuesEnum?
    var presenter: CreateNewPasswordPresenter?

    /* setting up presenter */
    func setupPresenter(_ presenter: CreateNewPasswordPresenter) {
        self.presenter = presenter
    }

    /* setting up cell */
    func setupCell(_ title: String, value: String, valueType: PasswordValuesEnum) {
        self.valueType = valueType
        lblTitle.text = title
        tfValue.text = value
    }

    /* set hint text */
    func setHint(_ hint: String) {
        tfValue.placeholder = hint
    }

    /* value did change */
    @IBAction func valueChanged(_: Any) {
        switch valueType! {
        case .PasswordName:
            presenter?.setPasswordName(tfValue.text!)
            break
        case .PasswordEmail:
            presenter?.setPasswordEmailPin(tfValue.text!)
            break
        case .Password:
            presenter?.setPassword(tfValue.text!)
            break
        }
    }
}
