//
//  CreateNewPasswordInteractor.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation

protocol CreateNewPasswordInteractor {
    init(_ api: CreateNewPasswordApi)
    func savePassword(_ newPassword: DTOPassword, userPasswords: [String], completition: @escaping (_ success: Bool) -> Void)
    func updatePassword(_ oldPassword: DTOPassword, completition: @escaping (_ success: Bool) -> Void)
}
