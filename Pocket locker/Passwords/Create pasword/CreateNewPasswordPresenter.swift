//
//  CreateNewPasswordProtocol.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation

protocol CreateNewPasswordPresenter {
    init(_ view: ViewCreateNewPassword, interactor: CreateNewPasswordInteractor)
    func setUserPasswords(_ userPasswords: [String])
    func savePasswor()
    func updatePassword()
    func setPasswordName(_ name: String)
    func setPasswordEmailPin(_ email: String)
    func setPassword(_ password: String)
    func setPasswordId(_ id: String)
    func getPasswordName() -> String
    func getPasswordEmail() -> String
    func getPassword() -> String
    func setRandomPassword()
    func isPasswordUpdating() -> Bool
    func setPasswordUpdating()
    func showUpdatePasswordDialog()
}
