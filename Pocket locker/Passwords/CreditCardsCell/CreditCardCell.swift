//
//  PasswordCell.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/6/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

class CreditCardCell: UITableViewCell {
    @IBOutlet var logoImg: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPin: UITextView!

    func setupCell(_ name: String, pin: String) {
        lblName.text = name
        lblPin.text = pin
        createLogo(name)
    }

    /* creating logo */
    internal func createLogo(_ name: String) {
        if name.uppercased().contains("VISA") {
            logoImg.text = "V"
            setupColors(Constants.COLOR_VISA, titleColor: Constants.COLOR_VISA_TITLE)
        } else if name.uppercased().contains("MASTER") {
            logoImg.text = "M"
            setupColors(Constants.COLOR_MASTER_CARD, titleColor: Constants.COLOR_MASTER_CARD_TITLE)
        } else if name.uppercased().contains("EXPRESS") {
            logoImg.text = "A"
            setupColors(Constants.COLOR_AMERICAN_EXPRESS, titleColor: Constants.COLOR_AMERICAN_EXPRESS_TITLE)
        } else if name.uppercased().contains("GOLD") {
            logoImg.text = "G"
            setupColors(Constants.COLOR_AMERICAN_GOLD, titleColor: Constants.COLOR_AMERICAN_GOLD_TITLE)
        } else if name.uppercased().contains("DINERS") {
            logoImg.text = "D"
            setupColors(Constants.COLOR_DINERS, titleColor: Constants.COLOR_DINERS_TITLE)
        } else {
            let index = name.index(name.startIndex, offsetBy: 0)
            logoImg.text = "\(name[index])"
            setupColors(Constants.COLOR_OTHER_CARDS, titleColor: Constants.COLOR_OTHER_CARDS_TITLE)
        }
        logoImg.layer.masksToBounds = true
        logoImg.layer.cornerRadius = 20.0
    }

    /* setting up colors for logo */
    internal func setupColors(_ backgroundColor: UIColor, titleColor: UIColor) {
        logoImg.textColor = titleColor
        logoImg.highlightedTextColor = titleColor
        logoImg.backgroundColor = backgroundColor
    }
}
