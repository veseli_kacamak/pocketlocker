//
//  AllPasswordsView.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

protocol AllPasswordsView: class {
    func showAllPasswordsAndCreditCards()
    func showError(_ errorMessage: String)
    func refreshCreditCardsSection()
    func refreshPasswordsSection()
    func logoutUser(_ alert: UIAlertController)
    func successLogout()
    func resetSearcBar()
    func showDeleteAlert(_ alert: UIAlertController)
    func editPassword(_ passwordId: String, selectedPassword: DTOPassword, userPasswordIds: [String])
    func editCreditCard(_ creditCardId: String, selectedCreditCard: DTOCard, userCreditCardIds: [String])
    func newCreditCard()
    func newPassword()
    func showEmptyMessage()
    func hideEmptyMessage()
}
