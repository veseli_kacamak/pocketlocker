//
//  AllPasswordsPresenter.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

protocol AllPasswordsPresenter {
    init(_ view: AllPasswordsView, interactor: AllPasswordsInteractor, lockerListener:LockerChangesListener)
    func getUserLocker()
    func getLockerPasswords() -> [DTOPassword]
    func getCreditCards() -> [DTOCard]
    func getUserCreditCardIds() -> [String]
    func getUserPasswordIds() -> [String]
    func deletePassword(_ passwordId: String)
    func deleteCreditCard(_ creditCardId: String)
    func logoutUser()
    func logoutAction()
    func search(_ serachTerm: String)
    func resetSearchBar()
    func deletePasswordAlert(_ secureId: String)
    func deleteCreditCardAlert(_ secureId: String)
    func openCreditCard(_ index: Int)
    func openPassword(_ index: Int)
    func newCreditCard()
    func newPassword()
}
