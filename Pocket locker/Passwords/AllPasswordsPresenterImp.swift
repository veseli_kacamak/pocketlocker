//
//  AllPasswordsPresenterImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import FirebaseAuth
import Foundation
import UIKit

class AllPasswordsPresenterImp: AllPasswordsPresenter {
    unowned var view: AllPasswordsView
    var interactor: AllPasswordsInteractor
    var lockerListener: LockerChangesListener
    private var creditCards = [DTOCard]()
    private var passwords = [DTOPassword]()
    private var userCreditCardIds = [String]()
    private var userPasswordIds = [String]()
    private var searchTerm = ""

    required init(_ view: AllPasswordsView, interactor: AllPasswordsInteractor, lockerListener: LockerChangesListener) {
        self.view = view
        self.interactor = interactor
        self.lockerListener = lockerListener
    }

    /* gettng user locker */
    func getUserLocker() {
        let userId = Auth.auth().currentUser?.uid
        interactor.getUserLocker(userId != nil ? userId! : "", completion: { lockerResult, error in
            guard let locker = lockerResult else {
                if let lockerError = error {
                    self.view.showError(lockerError.errorMessage)
                    return
                }
                return
            }
            
            self.removeAllData()
            if !locker.cards.isEmpty || !locker.passwords.isEmpty {
                self.view.hideEmptyMessage()
                self.setupPasswords(locker.passwords)
                self.setupCreditCards(locker.cards)
            }
            else {
                self.view.showEmptyMessage()
            }
            
        })
    }

    /* removing all data from passwords and credit cards */
    internal func removeAllData() {
        passwords.removeAll()
        creditCards.removeAll()
        userPasswordIds.removeAll()
        userCreditCardIds.removeAll()
        view.showAllPasswordsAndCreditCards()
    }

    /* getting password for passwordId */
    internal func getPasswordFor(_ passwordId: String) {
        interactor.getPassword(passwordId, completion: { password, error in
            guard let lockerPassword = password else {
                if let lockerError = error {
                    self.view.showError(lockerError.errorMessage)
                    return
                }
                return
            }
            
            self.insertPassword(lockerPassword)
            self.view.refreshPasswordsSection()
        })
    }

    /* getting credit card for creditCardId */
    internal func getCreditCardFor(_ creditCardId: String) {
        interactor.getCreditCard(creditCardId, completion: { creditCard, error in
            guard let lockerCreditCard = creditCard else {
                if let lockerError = error {
                    self.view.showError(lockerError.errorMessage)
                    return
                }
                return
            }
            
            self.insertCards(lockerCreditCard)
            self.view.refreshCreditCardsSection()
        })
    }

    /* setting up credit card ids and list of credit cards */
    internal func setupCreditCards(_ cards: [String]) {
        userCreditCardIds = cards
        addCreditCards(cards)
    }

    /* adding credit cards to list */
    internal func addCreditCards(_ cards: [String]) {
        for card in cards {
            getCreditCardFor(card)
        }
    }

    /* setting up password ids and list of passwords */
    internal func setupPasswords(_ lockerPasswords: [String]) {
        userPasswordIds = lockerPasswords
        addPasswords(lockerPasswords)
    }

    /* adding passwords to list */
    internal func addPasswords(_ lockerPasswords: [String]) {
        for password in lockerPasswords {
            getPasswordFor(password)
        }
    }

    /* getting all passwords (filtered or not) */
    func getLockerPasswords() -> [DTOPassword] {
        return searchTerm.isEmpty ? passwords : filterPasswords()
    }

    /* getting all credit cards (filtered or not) */
    func getCreditCards() -> [DTOCard] {
        return searchTerm.isEmpty ? creditCards : filterCreditCards()
    }

    /* getting all credit card ids */
    func getUserCreditCardIds() -> [String] {
        return userCreditCardIds
    }

    /* getting all password ids */
    func getUserPasswordIds() -> [String] {
        return userPasswordIds
    }

    /* insert password to the list */
    internal func insertPassword(_ password: DTOPassword) {
        if !passwords.contains(password) {
            passwords.append(password)
        } else {
            let index = passwords.index(of: password)
            passwords[index!] = password
        }
    }

    /* filtered passwor lits */
    internal func filterPasswords() -> [DTOPassword] {
        // filter by search term
        if !searchTerm.isEmpty {
            return passwords.filter { $0.name.uppercased().contains(searchTerm.uppercased()) }
        } else {
            return passwords
        }
    }

    /* insert card to list */
    internal func insertCards(_ card: DTOCard) {
        if !creditCards.contains(card) {
            creditCards.append(card)
        } else {
            let index = creditCards.index(of: card)
            creditCards[index!] = card
        }
    }

    /* filtered credi card list */
    internal func filterCreditCards() -> [DTOCard] {
        // filter by search term
        if !searchTerm.isEmpty {
            return creditCards.filter { $0.name.uppercased().contains(searchTerm.uppercased()) }
        } else {
            return creditCards
        }
    }

    /* deleting password for passwordId */
    func deletePassword(_ passwordId: String) {
        interactor.deletePassword(passwordId, userPasswords: userPasswordIds, completion: {
            _ in
            self.lockerListener.deletedPassword()
        })
    }

    /* delete credic card for creditCardId */
    func deleteCreditCard(_ creditCardId: String) {
        interactor.deleteCreditCard(creditCardId, userCards: userCreditCardIds, completion: {
            _ in
            self.lockerListener.deletedCard()
        })
    }

    /* create delete password alert */
    func deletePasswordAlert(_ secureId: String) {
        let title = "Delete password"
        let message = "Are you sure that you want to delete password?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (_: UIAlertAction!) in
        }
        let DeleteAction = UIAlertAction(title: "Delete", style: .default) { (_: UIAlertAction!) in
            self.deletePassword(secureId)
        }
        alertController.addAction(DeleteAction)
        alertController.addAction(CancelAction)
        view.showDeleteAlert(alertController)
    }

    /* create delete credit card alert */
    func deleteCreditCardAlert(_ secureId: String) {
        let title = "Delete credit card"
        let message = "Are you sure that you want to delete credit card?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (_: UIAlertAction!) in
        }
        let DeleteAction = UIAlertAction(title: "Delete", style: .default) { (_: UIAlertAction!) in
            self.deleteCreditCard(secureId)
        }
        alertController.addAction(DeleteAction)
        alertController.addAction(CancelAction)
        view.showDeleteAlert(alertController)
    }

    /* logout user */
    func logoutUser() {
        let title = "Logout"
        let message = "Are you sure that you want to logout?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (_: UIAlertAction!) in
        }
        let LogoutAction = UIAlertAction(title: "Logout", style: .default) { (_: UIAlertAction!) in
            self.logoutAction()
        }
        alertController.addAction(LogoutAction)
        alertController.addAction(CancelAction)
        
        view.logoutUser(alertController)
    }

    /* successfull logout action */
    func logoutAction() {
        SharedUser.userLoggedIn(false)
        view.successLogout()
    }

    /* reset search bar */
    func resetSearchBar() {
        searchTerm = ""
        view.resetSearcBar()
    }

    /* search by term */
    func search(_ serachTerm: String) {
        searchTerm = serachTerm
        view.showAllPasswordsAndCreditCards()
    }
    
    /* edit password */
    func openPassword(_ index: Int) {
        let passwordId = userPasswordIds[index]
        let selectedPassword =  passwords[index]
        self.view.editPassword(passwordId, selectedPassword: selectedPassword, userPasswordIds: userPasswordIds)
    }
    
    /* edit credit card */
    func openCreditCard(_ index: Int) {
        let creditCardId = userCreditCardIds[index]
        let selectedCreditCard = creditCards[index]
        self.view.editCreditCard(creditCardId, selectedCreditCard: selectedCreditCard, userCreditCardIds: userCreditCardIds)
    }
    
    /* open new credit card */
    func newCreditCard() {
        view.newCreditCard()
    }
    
    /* open new password */
    func newPassword() {
        view.newPassword()
    }
}
