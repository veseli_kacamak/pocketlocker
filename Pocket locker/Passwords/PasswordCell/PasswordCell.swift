//
//  PasswordCell.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/6/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

class PasswordCell: UITableViewCell {
    @IBOutlet var logoImg: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPassword: UITextView!

    func setupCell(_ name: String, password: String) {
        lblName.text = name
        lblPassword.text = password
        createLogo(name)
    }

    /* creating logo */
    internal func createLogo(_ name: String) {
        if name.uppercased().contains("FACEBOOK") {
            logoImg.text = "F"
            setupColors(Constants.COLOR_FACEBOOK, titleColor: Constants.COLOR_FACEBOOK_TITLE)
        } else if name.uppercased().contains("GOOGLE") || name.uppercased().contains("GMAIL") {
            logoImg.text = "G"
            setupColors(Constants.COLOR_GMAIL, titleColor: Constants.COLOR_GMAIL_TITLE)
        } else if name.uppercased().contains("YAHOO") {
            logoImg.text = "Y"
            setupColors(Constants.COLOR_YAHOO, titleColor: Constants.COLOR_YAHOO_TITLE)
        } else if name.uppercased().contains("AMAZON") {
            logoImg.text = "A"
            setupColors(Constants.COLOR_AMAZON, titleColor: Constants.COLOR_AMAZON_TITLE)
        } else if name.uppercased().contains("PAY") {
            logoImg.text = "P"
            setupColors(Constants.COLOR_PAYPAL, titleColor: Constants.COLOR_PAYPAL_TITLE)
        } else if name.uppercased().contains("LINKEDIN") {
            logoImg.text = "L"
            setupColors(Constants.COLOR_LINKEDIN, titleColor: Constants.COLOR_LINKEDIN_TITLE)
        } else if name.uppercased().contains("ICLOUD") {
            logoImg.text = "iC"
            setupColors(Constants.COLOR_ICLOUD, titleColor: Constants.COLOR_ICLOUD_TITLE)
        } else if name.uppercased().contains("APPLE") {
            logoImg.text = "A"
            setupColors(Constants.COLOR_APPLE_ID, titleColor: Constants.COLOR_APPLE_ID_TITLE)
        } else if name.uppercased().contains("PREMIER") {
            logoImg.text = "P"
            setupColors(Constants.COLOR_PREMIER_LEAGUE_FM, titleColor: Constants.COLOR_PREMIER_LEAGUE_FM_TITLE)
        } else if name.uppercased().contains("TWITTER") {
            logoImg.text = "T"
            setupColors(Constants.COLOR_TWITTER, titleColor: Constants.COLOR_TWITTER_TITLE)
        } else {
            let index = name.index(name.startIndex, offsetBy: 0)
            logoImg.text = "\(name[index])"
            setupColors(Constants.COLOR_OTHER_PASSWORDS, titleColor: Constants.COLOR_OTHER_PASSWORDS_TITLE)
        }
        logoImg.layer.masksToBounds = true
        logoImg.layer.cornerRadius = 20.0
    }

    /* setting up colors for logo */
    internal func setupColors(_ backgroundColor: UIColor, titleColor: UIColor) {
        logoImg.textColor = titleColor
        logoImg.highlightedTextColor = titleColor
        logoImg.backgroundColor = backgroundColor
    }
}
