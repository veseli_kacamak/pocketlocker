//
//  AllPasswordsAPIImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import Foundation

class AllPasswordsAPIImp: AllPasswordsAPI {
    let networkController: FBDBController

    required init(_ networkController: FBDBController) {
        self.networkController = networkController
        self.networkController.ref.keepSynced(true)
    }

    /* getting user locker for userId */
    func getUserLocker(_ userId: String, completition: @escaping (Locker?, LockerError?) -> Void) {
        networkController.ref.child("lockers").child(userId).observeSingleEvent(of: .value, with: { snapshot in
            let postDict = snapshot.value as? [String: AnyObject] ?? [:]
            let locker = Locker(fromDictionary: postDict as NSDictionary)
            completition(locker, nil)
        }) { error in
            print(error.localizedDescription)
            completition(nil, LockerError.init(error.localizedDescription, errorCode: ""))
        }
    }

    /* get password for passwordId */
    func getPassword(_ passwordId: String, completition: @escaping (DTOPassword?, LockerError?) -> Void) {
        networkController.ref.child("passwords").child(passwordId).observeSingleEvent(of: .value, with: { snapshot in
            let postDict = snapshot.value as? [String: AnyObject] ?? [:]
            let password = DTOPassword(fromDictionary: postDict as NSDictionary)
            completition(password, nil)
        }) { error in
            print(error.localizedDescription)
            completition(nil, LockerError.init(error.localizedDescription, errorCode: ""))
        }
    }

    /* get credit card for creditCardId */
    func getCreditCard(_ creditCardId: String, completition: @escaping (DTOCard?, LockerError?) -> Void) {
        networkController.ref.child("cards").child(creditCardId).observeSingleEvent(of: .value, with: { snapshot in
            let postDict = snapshot.value as? [String: AnyObject] ?? [:]
            let password = DTOCard(fromDictionary: postDict as NSDictionary)
            completition(password, nil)
        }) { error in
            print(error.localizedDescription)
            completition(nil, LockerError.init(error.localizedDescription, errorCode: ""))
        }
    }

    /* deleting password for passwordId */
    func deletePassword(_ passwordId: String, userPasswords: [String], completition: @escaping (Bool) -> Void) {
        let userId = Auth.auth().currentUser?.uid ?? ""
        var newUserPasswords = userPasswords
        let index = newUserPasswords.index(of: passwordId)
        newUserPasswords.remove(at: index!)

        var passwords = [String: Any]()
        if !newUserPasswords.isEmpty {
            for i in 0 ... newUserPasswords.count - 1 {
                passwords["\(i)"] = newUserPasswords[i]
            }
        }

        let childUpdates = [
            "/passwords/\(passwordId)/": nil,
            "/lockers/\(userId)/passwords/": passwords,
        ]
        networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
            completition(true)
        })
    }

    /* delete credit card for creditCardId */
    func deleteCreditCard(_ creditCardId: String, userCards: [String], completition: @escaping (Bool) -> Void) {
        let userId = Auth.auth().currentUser?.uid ?? ""
        var newUserCards = userCards
        let index = newUserCards.index(of: creditCardId)
        newUserCards.remove(at: index!)

        var cards = [String: Any]()
        if !newUserCards.isEmpty {
            for i in 0 ... newUserCards.count - 1 {
                cards["\(i)"] = newUserCards[i]
            }
        }

        let childUpdates = [
            "/cards/\(creditCardId)/": nil,
            "/lockers/\(userId)/cards/": cards,
        ]
        networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
            completition(true)
        })
    }
}
