//
//  AllPasswordsAPI.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import RxSwift

protocol AllPasswordsAPI {
    init(_ networkController: FBDBController)
    func getUserLocker(_ userId: String, completition: @escaping (_ result: Locker?, _ error: LockerError?) -> Void)
    func getPassword(_ passwordId: String, completition: @escaping (_ result: DTOPassword?, _ error: LockerError?) -> Void)
    func getCreditCard(_ creditCardId: String, completition: @escaping (_ result: DTOCard?, _ error: LockerError?) -> Void)
    func deleteCreditCard(_ creditCardId: String, userCards: [String], completition: @escaping (Bool) -> Void)
    func deletePassword(_ passwordId: String, userPasswords: [String], completition: @escaping (Bool) -> Void)
}
