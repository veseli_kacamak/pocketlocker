//
//  AppDelegate.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/6/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Crashlytics
import Fabric
import Firebase
import GoogleSignIn
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    var window: UIWindow?
    let MASK_VIEW_TAG = 1234

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Use Firebase library to configure APIs
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        SharedUser.lockerLocked(true)
        return true
    }

    @available(iOS 9.0, *)
    func application(_: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any])
        -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }

    func sign(_: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            print("sign in error = \(error.localizedDescription)")
            return
        }

        guard let authentication = user.authentication else { return }
        _ = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                          accessToken: authentication.accessToken)
        // ...
    }

    func sign(_: GIDSignIn!, didDisconnectWith _: GIDGoogleUser!, withError _: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func applicationWillResignActive(_: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        // self.window?.isHidden = true
        maskWindow()
    }

    func applicationDidEnterBackground(_: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if SharedUser.isUserLoggedIn() && !SharedUser.isAuthenticationNeeded() {
            SharedUser.needAuthentication(true)
        }
    }

    func applicationWillEnterForeground(_: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        unmaskWindow()

        // When user go out from app and return, he will be redirect to authenticate page because of security
        if SharedUser.isUserLoggedIn() && SharedUser.isAuthenticationNeeded() && !SharedUser.isLockerLocked() {
            SharedUser.needAuthentication(false)
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                let signUpViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as UIViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(signUpViewController, animated: true, completion: nil)
            }
        }
    }

    func applicationWillTerminate(_: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        SharedUser.lockerLocked(true)
    }

    internal func maskWindow() {
        let colorView = UIView(frame: (window?.frame)!)
        colorView.backgroundColor = UIColor.white
        colorView.tag = MASK_VIEW_TAG
        colorView.alpha = 0
        window?.addSubview(colorView)
        window?.bringSubview(toFront: colorView)

        UIView.animate(withDuration: 0.5, animations: {
            colorView.alpha = 1
        }, completion: nil)
    }

    internal func unmaskWindow() {
        if let colorView = self.window?.viewWithTag(MASK_VIEW_TAG) {
            UIView.animate(withDuration: 0.5, animations: {
                colorView.alpha = 0
            }, completion: {
                (_: Bool) in
                colorView.removeFromSuperview()
            })
        }
    }
}
