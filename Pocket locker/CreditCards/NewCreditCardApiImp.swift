//
//  NewCreditCardApiImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import Foundation

class NewCreditCardApiImp: NewCreditCardApi {
    var networkController: FBDBController

    required init(_ networkController: FBDBController) {
        self.networkController = networkController
        self.networkController.ref.keepSynced(true)
    }

    /* save credit card */
    func saveCreditCard(_ creditCard: DTOCard, userCards: [String], completition: @escaping (Bool) -> Void) {
        if let cardId = creditCard.cardId {
            let userId = Auth.auth().currentUser?.uid ?? ""

            let card = [
                "description": creditCard.cardDescription!,
                "name": creditCard.name!,
                "pin": creditCard.pin!,
            ]

            var cards = [String: Any]()
            if !userCards.isEmpty {
                for i in 0 ... userCards.count - 1 {
                    cards["\(i)"] = userCards[i]
                }
            }

            // add new value
            let cardsSize = cards.count
            let oldCardIndex = cardsSize - 1
            let newCardIndex = oldCardIndex + 1 < 0 ? 0 : oldCardIndex + 1
            cards["\(newCardIndex)"] = "\(cardId)"

            let childUpdates = [
                "/cards/\(cardId)/": card,
                "/lockers/\(userId)/cards/": cards,
            ]
            networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
                completition(true)
            })
        }
    }

    /* update credit card */
    func upadteCreditCard(_ creditCard: DTOCard, completition: @escaping (Bool) -> Void) {
        if let cardId = creditCard.cardId {
            let card = [
                "description": creditCard.cardDescription!,
                "name": creditCard.name!,
                "pin": creditCard.pin!,
            ]

            let childUpdates = ["/cards/\(cardId)/": card]
            networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
                completition(true)
            })
        }
    }
}
