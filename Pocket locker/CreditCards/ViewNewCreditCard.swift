//
//  ViewNewCreditCard.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

protocol ViewNewCreditCard: class {
    func successfullySavedCreditCard()
    func successfullyUpdateCreditCard()
    func showAlert(_ alert: UIAlertController)
    func error(_ errorMessage: String)
}
