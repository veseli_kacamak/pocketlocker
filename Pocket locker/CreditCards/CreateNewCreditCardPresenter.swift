//
//  CreateNewCreditCardPresenter.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol CreateNewCreditCardPresenter {
    init(_ view: ViewNewCreditCard, interactor: InteractorNewCreditCard)
    func setUserCards(_ userCards: [String])
    func saveCreditCard()
    func updateCreditCard()
    func setCreditCardName(_ name: String)
    func setCreditCardPin(_ pin: String)
    func setCreditCardId(_ id: String)
    func getCreditCardName() -> String
    func getCreditCardPin() -> String
    func isCreditCardUpdating() -> Bool
    func setCreditCardUpdating()
    func showUpdateCreditCardDialog()
}
