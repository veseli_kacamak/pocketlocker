//
//  InteractorNewCreditCardImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class InteractorNewCreditCardImp: InteractorNewCreditCard {
    var api: NewCreditCardApi

    required init(_ api: NewCreditCardApi) {
        self.api = api
    }

    /* save credit card */
    func saveCreditCard(_ newCreditCard: DTOCard, userCards: [String], completition: @escaping (_ success: Bool) -> Void) {
        api.saveCreditCard(newCreditCard, userCards: userCards, completition: { result in
            completition(result)
        })
    }

    /* update credit card */
    func updateCreditCard(_ oldCreditCard: DTOCard, completition: @escaping (Bool) -> Void) {
        api.upadteCreditCard(oldCreditCard, completition: { result in
            completition(result)
        })
    }
}
