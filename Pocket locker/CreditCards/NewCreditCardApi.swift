//
//  NewCreditCardApi.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol NewCreditCardApi {
    init(_ networkController: FBDBController)
    func saveCreditCard(_ creditCard: DTOCard, userCards: [String], completition: @escaping (Bool) -> Void)
    func upadteCreditCard(_ creditCard: DTOCard, completition: @escaping (Bool) -> Void)
}
