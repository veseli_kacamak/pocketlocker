//
//  CreateNewCreditCardPresenterImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

class CreateNewCreditCardPresenterImp: CreateNewCreditCardPresenter {
    unowned var view: ViewNewCreditCard
    var interactor: InteractorNewCreditCard
    var creditCardName = ""
    var creditCardPin = ""
    var creditCardId = ""
    var updatingCreditCard = false
    var userCards = [String]()

    required init(_ view: ViewNewCreditCard, interactor: InteractorNewCreditCard) {
        self.view = view
        self.interactor = interactor
    }

    /* set user cards */
    func setUserCards(_ userCards: [String]) {
        self.userCards = userCards
    }

    /* save credit card */
    func saveCreditCard() {
        if creditCardName.count > 0 && creditCardPin.count > 0 {
            let newCreditCard = DTOCard(Utils.sharedInstance.cardAndPasswordIdGenerator(length: 8), description: "description", name: creditCardName, pin: creditCardPin)
            interactor.saveCreditCard(newCreditCard, userCards: userCards, completition: { result in
                result ? self.view.successfullySavedCreditCard() : self.view.error("failed to save credit card!")
            })
        } else {
            view.error("You must fill all data!!!")
        }
    }

    /* update credit card */
    func updateCreditCard() {
        if creditCardName.count > 0 && creditCardPin.count > 0 {
            let oldCreditCard = DTOCard(creditCardId, description: "description", name: creditCardName, pin: creditCardPin)
            interactor.updateCreditCard(oldCreditCard, completition: { result in
                result ? self.view.successfullyUpdateCreditCard() : self.view.error("failed to save credit card!")
            })
        } else {
            view.error("You must fill all data!!!")
        }
    }

    /* show update credit card dialog */
    func showUpdateCreditCardDialog() {
        let title = "Update credit card"
        let message = "Are you sure that you want to update credit card?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (_: UIAlertAction!) in
        }
        let DeleteAction = UIAlertAction(title: "Update", style: .default) { (_: UIAlertAction!) in
            self.updateCreditCard()
        }
        alertController.addAction(DeleteAction)
        alertController.addAction(CancelAction)
        view.showAlert(alertController)
    }

    /* set credit card name */
    func setCreditCardName(_ name: String) {
        creditCardName = name
    }

    /* set credit card pin */
    func setCreditCardPin(_ pin: String) {
        creditCardPin = pin
    }

    /* set credit card id */
    func setCreditCardId(_ id: String) {
        creditCardId = id
    }

    /* set credit card name */
    func getCreditCardName() -> String {
        return creditCardName
    }

    /* set credit card pin */
    func getCreditCardPin() -> String {
        return creditCardPin
    }

    /* check if credit card currently updating */
    func isCreditCardUpdating() -> Bool {
        return updatingCreditCard
    }

    /* set is credit card updating flag */
    func setCreditCardUpdating() {
        updatingCreditCard = true
    }
}
