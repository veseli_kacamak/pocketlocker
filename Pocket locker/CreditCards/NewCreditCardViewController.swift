//
//  NewCreditCardViewController.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/29/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

class NewCreditCardViewController: UIViewController, ViewNewCreditCard {
    @IBOutlet var tableView: UITableView!
    let NEW_CREDIT_CARD_CELL_ID = "NewCreditCardCell"
    let TITLE_NAME = "Card name"
    let TITLE_PIN = "PIN"
    let ERROR_ALERT_TITLE = "Error"
    let RIGHT_BUTTON_NAME = "Save"
    var nameValue = ""
    var pinValue = ""
    var creditCardId = ""
    var userCreditCards = [String]()
    var presenter: CreateNewCreditCardPresenter?
    var lockerChangesListener: LockerChangesListener?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarButtons()
        setupPresenter()
        setupUI()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    /* setup navigation bar buttons */
    func setupNavigationBarButtons() {
        let save = UIBarButtonItem(title: RIGHT_BUTTON_NAME, style: .plain, target: self, action: #selector(saveCreditCardAction))
        navigationItem.rightBarButtonItems = [save]
    }

    /* setup presenter */
    func setupPresenter() {
        InjectorHelper.inject(self)
        presenter?.setUserCards(userCreditCards)
        if nameValue.count > 0 && pinValue.count > 0 && creditCardId.count > 0 {
            presenter?.setCreditCardUpdating()
            presenter?.setCreditCardName(nameValue)
            presenter?.setCreditCardPin(pinValue)
            presenter?.setCreditCardId(creditCardId)
        }
    }

    /* setup UI */
    func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        /* setting table view cell */
        tableView.register(UINib(nibName: NEW_CREDIT_CARD_CELL_ID, bundle: nil), forCellReuseIdentifier: NEW_CREDIT_CARD_CELL_ID)
    }

    /* successfully saved credit card */
    func successfullySavedCreditCard() {
        lockerChangesListener?.savedCard()
        navigationController?.popViewController(animated: false)
    }

    /* succesfully update credit card */
    func successfullyUpdateCreditCard() {
        lockerChangesListener?.updatedCard()
        navigationController?.popViewController(animated: false)
    }

    /* show error dialog */
    func error(_ errorMessage: String) {
        Utils.sharedInstance.createAlert(self, title: ERROR_ALERT_TITLE, message: errorMessage)
    }

    /* show alert */
    func showAlert(_ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }

    /* save credit card action */
    @objc func saveCreditCardAction(_: Any) {
        (presenter?.isCreditCardUpdating())! ? presenter?.showUpdateCreditCardDialog() : presenter?.saveCreditCard()
    }
}

extension NewCreditCardViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NEW_CREDIT_CARD_CELL_ID, for: indexPath) as! NewCreditCardCell
        cell.setupPresenter(presenter!)
        switch indexPath.row {
        case 0:
            cell.setHint("Card name")
            cell.setupCell(TITLE_NAME, value: (presenter?.getCreditCardName())!, valueType: .CreditCardName)
            break
        case 1:
            cell.setHint("****")
            cell.setPinField()
            cell.setupCell(TITLE_PIN, value: (presenter?.getCreditCardPin())!, valueType: .CreditCardPin)
            break
        default:
            break
        }
        return cell
    }
}
