//
//  NewCreditCardCell.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation
import UIKit

class NewCreditCardCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet var tfValue: UITextField!
    @IBOutlet var lblTitle: UILabel!
    var valueType: CreditCardValuesEnum?
    var presenter: CreateNewCreditCardPresenter?
    let pinMaxCharacters = 4

    /* setup presenter */
    func setupPresenter(_ presenter: CreateNewCreditCardPresenter) {
        self.presenter = presenter
        tfValue.delegate = self
    }

    /* setup cell */
    func setupCell(_ title: String, value: String, valueType: CreditCardValuesEnum) {
        self.valueType = valueType
        lblTitle.text = title
        tfValue.text = value
    }

    /* set hint text */
    func setHint(_ hint: String) {
        tfValue.placeholder = hint
    }

    /* set pin field */
    func setPinField() {
        tfValue.keyboardType = .numberPad
    }

    /* value changed action */
    @IBAction func valueChanged(_: Any) {
        switch valueType! {
        case .CreditCardName:
            presenter?.setCreditCardName(tfValue.text!)
            break
        case .CreditCardPin:
            if tfValue.text!.count <= 4 {
                presenter?.setCreditCardPin(tfValue.text!)
            }
            break
        }
    }

    /* checking pin character range */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if valueType == .CreditCardPin {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= pinMaxCharacters
        } else {
            return true
        }
    }
}
