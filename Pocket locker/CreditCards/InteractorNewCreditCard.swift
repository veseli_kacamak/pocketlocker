//
//  InteractorNewCreditCard.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/2/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol InteractorNewCreditCard {
    init(_ api: NewCreditCardApi)
    func saveCreditCard(_ newCreditCard: DTOCard, userCards: [String], completition: @escaping (_ success: Bool) -> Void)
    func updateCreditCard(_ oldCreditCard: DTOCard, completition: @escaping (_ success: Bool) -> Void)
}
