//
//  LoginAPI.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/6/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol LoginAPI {
    init(_ networkController: FBDBController)
    func signupUser(_ completition: @escaping (Bool) -> Void)
}
