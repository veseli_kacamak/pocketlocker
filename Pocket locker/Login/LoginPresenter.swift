//
//  LoginPresenter.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol LoginPresenter {
    init(_ view: ViewLogin, interactor: LoginInteractor)
    func login()
    func logout()
    func showError(_ error: Error)
    func goToUserLocker()
    func authenticateUser()
    func tryAgainToAuthenticateser()
    func checkIfUserLoggedIn()
    func setUserLoggedIn()
    func register()
}
