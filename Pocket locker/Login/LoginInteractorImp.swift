//
//  LoginInteractorImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/6/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class LoginInteractorImp: LoginInteractor {
    var api: LoginAPI

    required init(_ api: LoginAPI) {
        self.api = api
    }

    /* user sign up */
    func signupUser(_ completition: @escaping (Bool) -> Void) {
        api.signupUser({ result in
            completition(result)
        })
    }
}
