//
//  LoginInteractor.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/6/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol LoginInteractor {
    init(_ api: LoginAPI)
    func signupUser(_ completition: @escaping (Bool) -> Void)
}
