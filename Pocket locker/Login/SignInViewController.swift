//
//  SignInViewController.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 7/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import FirebaseAuth
import Foundation
import GoogleSignIn
import UIKit

class SignInViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, ViewLogin, UIAlertViewDelegate {
    @IBOutlet var signInBtn: GIDSignInButton!
    @IBOutlet var tryAgain: UIButton!
    @IBOutlet var progressBar: UIActivityIndicatorView!
    var presenter: LoginPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresenter()
        setupGID()
        setupUI()
    }

    /* setting up presenter */
    func setupPresenter() {
        InjectorHelper.inject(self)
    }

    /* setting UI */
    func setupUI() {
        presenter?.checkIfUserLoggedIn()
    }

    /* setting Google authentification */
    func setupGID() {
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }

    /* show sign in button */
    func showSignInButton() {
        signInBtn.isHidden = false
    }

    /* hide sign in button */
    func hideSignInButton() {
        signInBtn.isHidden = true
    }

    /* show try again button */
    func showTryAgainButton() {
        tryAgain.isHidden = false
    }

    /* hide try again button */
    func hideTryAgainButton() {
        tryAgain.isHidden = true
    }

    /* hide progress bar */
    func hideProgressBar() {
        progressBar.isHidden = true
        progressBar.stopAnimating()
    }

    /* show progress bar */
    func showProgressBar() {
        progressBar.isHidden = false
        progressBar.startAnimating()
    }

    /* show error alert with custom message */
    func showError(_ errorMessage: String) {
        Utils.sharedInstance.createAlert(self, title: "Login error", message: errorMessage)
    }

    /* sign in action */
    func sign(_: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            presenter?.showError(error)
            print("!!!! sign in error occured")
            return
        }

        guard let authentication = user.authentication else {
            print("AUTENTICATION ERROR - RETURN")
            return
        }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        // ...
        Auth.auth().signIn(with: credential) { user, error in
            if let error = error {
                self.presenter?.showError(error)
                print("error in sign in")
                return
            }

            print("USER \(String(describing: user?.displayName)) HAS SUCCESSFULL LOGED IN")
            SharedUser.userLoggedIn(true)
            // User is signed in
            // ...
            self.presenter?.register()
        }
    }

    /* go to user locker view controller */
    func goToUserLocker() {
        let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PasswordsNavigationController") as UIViewController
        present(viewController, animated: false, completion: nil)
    }

    func showPasswordAlert() {
        let passwordAlert: UIAlertView = UIAlertView(title: "TouchIDDemo", message: "Please type your password", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Okay")
        passwordAlert.alertViewStyle = UIAlertViewStyle.secureTextInput
        passwordAlert.show()
    }

    /* logout action */
    func sign(_: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError _: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        print("USER \(String(describing: user.userID)) HAS SUCCESSFULL LOGED OUT")
    }

    @IBAction func signInBtnAction(_: Any) {
        presenter?.login()
    }

    @IBAction func tryAgainButton(_: Any) {
        presenter?.tryAgainToAuthenticateser()
    }
}
