//
//  LoginAPIImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/6/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import Foundation

class LoginAPIImp: LoginAPI {
    var networkController: FBDBController

    required init(_ networkController: FBDBController) {
        self.networkController = networkController
        self.networkController.ref.keepSynced(true)
    }

    /* user sign up */
    func signupUser(_ completition: @escaping (Bool) -> Void) {
        let userId = Auth.auth().currentUser?.uid ?? ""
        let eMail = Auth.auth().currentUser?.email ?? ""

        let user = [
            "lockerId": userId,
            "mail": eMail,
        ]

        let childUpdates = ["/users/\(userId)/": user]
        networkController.ref.updateChildValues(childUpdates, withCompletionBlock: { _, _ in
            completition(true)
        })
    }
}
