//
//  ViewLogin.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

protocol ViewLogin: class {
    func showError(_ errorMessage: String)
    func goToUserLocker()
    func showPasswordAlert()
    func hideSignInButton()
    func showSignInButton()
    func showTryAgainButton()
    func hideTryAgainButton()
    func hideProgressBar()
    func showProgressBar()
}
