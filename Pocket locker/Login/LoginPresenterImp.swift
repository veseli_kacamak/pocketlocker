//
//  LoginPresenterImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/4/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import FirebaseAuth
import Foundation
import GoogleSignIn
import LocalAuthentication

class LoginPresenterImp: LoginPresenter {
    unowned var view: ViewLogin
    var interactor: LoginInteractor

    required init(_ view: ViewLogin, interactor: LoginInteractor) {
        self.view = view
        self.interactor = interactor
    }

    /* login */
    func login() {
        view.showProgressBar()
        GIDSignIn.sharedInstance().signIn()
    }

    /* logout */
    func logout() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            view.showError(signOutError.localizedDescription)
            print("Error signing out: %@", signOutError)
        }
    }

    /* show error */
    func showError(_ error: Error) {
        view.hideProgressBar()
        view.showError(error.localizedDescription)
    }

    /* go to user locker */
    func goToUserLocker() {
        view.goToUserLocker()
    }

    /* open authentification allert */
    func authenticateUser() {
        view.showTryAgainButton()

        let myContext = LAContext()
        let myLocalizedReasonString = "Use your fingerprint to open the locker."

        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, _ in
                    if success {
                        DispatchQueue.main.async {
                            // self.view.showProgressBar()
                            self.setUserLoggedIn()
                            self.goToUserLocker()
                        }
                    } else {
                        // User did not authenticate successfully, look at error and take appropriate action
                        DispatchQueue.main.async {
                            self.view.hideSignInButton()
                            self.view.showTryAgainButton()
                            self.view.hideProgressBar()
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                print("AUTH EVALUTE POLICY")
            }
        } else {
            // Fallback on earlier versions
            print("AUTH EARLY VERSIONS")
        }
    }

    /* user logged in validator */
    func checkIfUserLoggedIn() {
        if SharedUser.isUserLoggedIn() {
            view.hideSignInButton()
            authenticateUser()
        } else {
            view.hideProgressBar()
            view.showSignInButton()
            view.hideTryAgainButton()
        }
    }

    /* setting user logged in */
    func setUserLoggedIn() {
        SharedUser.needAuthentication(false)
        SharedUser.userLoggedIn(true)
        SharedUser.lockerLocked(false)
    }

    /* try again to authentificate */
    func tryAgainToAuthenticateser() {
        authenticateUser()
    }

    /* show password alert */
    func showPasswordAlert() {
        view.showPasswordAlert()
    }

    /* register */
    func register() {
        interactor.signupUser({ _ in
            self.goToUserLocker()
        })
    }
}
