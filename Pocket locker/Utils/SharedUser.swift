//
//  SharedUser.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 10/3/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class SharedUser {

    /* user logged in */
    static func userLoggedIn(_ isLoggedIn: Bool) {
        UserDefaults.standard.setValue(isLoggedIn, forKey: Constants.USER_LOGGED_IN)
        UserDefaults.standard.synchronize()
    }

    /* is user logged */
    static func isUserLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.USER_LOGGED_IN)
    }

    /* need for authentifications */
    static func needAuthentication(_ authentication: Bool) {
        UserDefaults.standard.setValue(authentication, forKey: Constants.USER_AUTHENTICATION)
        UserDefaults.standard.synchronize()
    }

    /* is authentification needeed */
    static func isAuthenticationNeeded() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.USER_AUTHENTICATION)
    }

    /* locker is locked */
    static func lockerLocked(_ lock: Bool) {
        UserDefaults.standard.setValue(lock, forKey: Constants.LOCKER_LOCKED)
        UserDefaults.standard.synchronize()
    }

    /* is locker locked */
    static func isLockerLocked() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.LOCKER_LOCKED)
    }
}
