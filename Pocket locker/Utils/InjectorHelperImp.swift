//
//  InjectorHelperImp.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

extension InjectorHelper {

    /* inject interactor and api for Sign in view controller */
    static func inject(_ controller: SignInViewController) {
        let api = LoginAPIImp(getFBDBController())
        let interactor = LoginInteractorImp(api)
        controller.presenter = LoginPresenterImp(controller, interactor: interactor)
    }

    /* inject interactor and api for Locker view controller */
    static func inject(_ controller: AllPasswordsTableViewController, lockerListener: LockerChangesListener) {
        let api = AllPasswordsAPIImp(getFBDBController())
        let interactor = AllPasswordsInteractorImp(api)
        controller.presenter = AllPasswordsPresenterImp(controller, interactor: interactor, lockerListener: lockerListener)
    }

    /* inject interactor and api for Creating password view controller */
    static func inject(_ controller: CreatePasswordViewController) {
        let api = CreateNewPasswordApiImp(getFBDBController())
        let interactor = CreateNewPasswordInteractorImp(api)
        controller.presenter = CreateNewPasswordPresenterImp(controller, interactor: interactor)
    }

    /* inject interactor and api fo Creating credit card view controller */
    static func inject(_ controller: NewCreditCardViewController) {
        let api = NewCreditCardApiImp(getFBDBController())
        let interactor = InteractorNewCreditCardImp(api)
        controller.presenter = CreateNewCreditCardPresenterImp(controller, interactor: interactor)
    }
}
