//
//  InjectorHelper.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Foundation

class InjectorHelper {

    static func getFBDBController() -> FBDBController {
        return FBDBController.sharedInstance
    }
}
