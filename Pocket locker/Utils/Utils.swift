//
//  Utils.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/12/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    static let sharedInstance = Utils()

    /* getting random alphanumeric for setted lenth */
    func randomString(length: Int) -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.!?#$%&"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }

    /* getting card and password id for lethh */
    func cardAndPasswordIdGenerator(length: Int) -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }

    /* creating alert with custom title and message */
    func createAlert(_ view: UIViewController, title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (_: UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        view.present(alertController, animated: true, completion: nil)
    }
}

extension UIViewController {

    /* hiding keyboard */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
