//
//  Logger.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 3/9/18.
//  Copyright © 2018 App society. All rights reserved.
//

import Foundation

class Logger{
    
    static let sharedInstance = Logger()
    
    func log(_ log: String, tag: String){
        print(String.init(format: "TAG : %@ - message: %@", tag, log))
    }
}
