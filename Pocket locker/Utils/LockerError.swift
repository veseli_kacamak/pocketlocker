//
//  LockerError.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 3/6/18.
//  Copyright © 2018 App society. All rights reserved.
//

import Foundation

struct LockerError {
    var errorMessage = ""
    var errorCode = ""
    
    init(_ errorMessage: String, errorCode: String) {
        self.errorMessage = errorMessage
        self.errorCode = errorCode
    }
}
