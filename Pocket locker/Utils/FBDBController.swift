//
//  FBDBController.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 9/11/17.
//  Copyright © 2017 App society. All rights reserved.
//

import Firebase
import Foundation

class FBDBController {
    static let sharedInstance = FBDBController()
    var ref: DatabaseReference = Database.database().reference()
}
