//
//  Constants.swift
//  Pocket locker
//
//  Created by Milan Bibeskovic on 12/17/16.
//  Copyright © 2016 App society. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    static let MASTERCARD = "MasterCard"
    static let VISA = "VISA"
    static let AMERICAN_EXPRESS = "American Express"

    // user defaults keys
    static let USER_APP_PIN = "USER_APP_PIN"
    static let USER_LOGGED_IN = "USER_LOGGED_IN"
    static let USER_AUTHENTICATION = "USER_AUTHENTICATION"
    static let LOCKER_LOCKED = "LOCKER_LOCKED"

    // color

    // passwords
    static let COLOR_FACEBOOK = UIColor(red: 60 / 255.0, green: 87 / 255.0, blue: 150 / 255.0, alpha: 1)
    static let COLOR_FACEBOOK_TITLE = UIColor.white
    static let COLOR_TWITTER = UIColor(red: 11 / 255.0, green: 170 / 255.0, blue: 235 / 255.0, alpha: 1)
    static let COLOR_TWITTER_TITLE = UIColor.white
    static let COLOR_GMAIL = UIColor(red: 213 / 255.0, green: 69 / 255.0, blue: 56 / 255.0, alpha: 1)
    static let COLOR_GMAIL_TITLE = UIColor.white
    static let COLOR_YAHOO = UIColor(red: 65 / 255.0, green: 0 / 255.0, blue: 141 / 255.0, alpha: 1)
    static let COLOR_YAHOO_TITLE = UIColor.white
    static let COLOR_AMAZON = UIColor.black
    static let COLOR_AMAZON_TITLE = UIColor(red: 255 / 255.0, green: 154 / 255.0, blue: 31 / 255.0, alpha: 1)
    static let COLOR_PAYPAL = UIColor(red: 3 / 255.0, green: 42 / 255.0, blue: 131 / 255.0, alpha: 1)
    static let COLOR_PAYPAL_TITLE = UIColor(red: 9 / 255.0, green: 154 / 255.0, blue: 220 / 255.0, alpha: 1)
    static let COLOR_LINKEDIN = UIColor(red: 6 / 255.0, green: 121 / 255.0, blue: 180 / 255.0, alpha: 1)
    static let COLOR_LINKEDIN_TITLE = UIColor.white
    static let COLOR_ICLOUD = UIColor(red: 98 / 255.0, green: 193 / 255.0, blue: 243 / 255.0, alpha: 1)
    static let COLOR_ICLOUD_TITLE = UIColor.white
    static let COLOR_APPLE_ID = UIColor.black
    static let COLOR_APPLE_ID_TITLE = UIColor.white
    static let COLOR_PREMIER_LEAGUE_FM = UIColor(red: 3 / 255.0, green: 255 / 255.0, blue: 143 / 255.0, alpha: 1)
    static let COLOR_PREMIER_LEAGUE_FM_TITLE = UIColor(red: 57 / 255.0, green: 0 / 255.0, blue: 61 / 255.0, alpha: 1)
    static let COLOR_OTHER_PASSWORDS = UIColor.gray
    static let COLOR_OTHER_PASSWORDS_TITLE = UIColor.white

    // credit cards
    static let COLOR_VISA = UIColor(red: 44 / 255.0, green: 51 / 255.0, blue: 145 / 255.0, alpha: 1)
    static let COLOR_VISA_TITLE = UIColor(red: 247 / 255.0, green: 150 / 255.0, blue: 36 / 255.0, alpha: 1)
    static let COLOR_MASTER_CARD = UIColor(red: 204 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 1)
    static let COLOR_MASTER_CARD_TITLE = UIColor(red: 254 / 255.0, green: 154 / 255.0, blue: 31 / 255.0, alpha: 1)
    static let COLOR_AMERICAN_EXPRESS = UIColor(red: 29 / 255.0, green: 148 / 255.0, blue: 205 / 255.0, alpha: 1)
    static let COLOR_AMERICAN_EXPRESS_TITLE = UIColor.white
    static let COLOR_AMERICAN_GOLD = UIColor(red: 193 / 255.0, green: 156 / 255.0, blue: 81 / 255.0, alpha: 1)
    static let COLOR_AMERICAN_GOLD_TITLE = UIColor.white
    static let COLOR_DINERS = UIColor(red: 39 / 255.0, green: 83 / 255.0, blue: 158 / 255.0, alpha: 1)
    static let COLOR_DINERS_TITLE = UIColor.white
    static let COLOR_OTHER_CARDS = UIColor.gray
    static let COLOR_OTHER_CARDS_TITLE = UIColor.white
}
