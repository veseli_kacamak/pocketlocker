//
//  PasswordsTest.swift
//  Pocket lockerTests
//
//  Created by Milan Bibeskovic on 3/18/18.
//  Copyright © 2018 App society. All rights reserved.
//

import XCTest

class PasswordsTest: XCTestCase {
    
    var view: ViewCreateNewPassword?
    var interactor: CreateNewPasswordInteractor?
    var api: CreateNewPasswordApi?
    var presenter: CreateNewPasswordPresenter?
    
    override func setUp() {
        super.setUp()
        api = MockCreateNewPasswordApi.init(FBDBController.sharedInstance)
        interactor = CreateNewPasswordInteractorImp.init(api!)
        view = MockViewCreatePassword()
        presenter = CreateNewPasswordPresenterImp.init(view!, interactor: interactor!)
    }
    
    override func tearDown() {
        api = nil
        interactor = nil
        view = nil
        presenter = nil
        super.tearDown()
    }
    
    func testSaveEmpty() {
        presenter?.savePasswor()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullySaved, "Password is saved!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setPasswordName("Password name")
        presenter?.savePasswor()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullySaved, "Password is saved!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setPasswordEmailPin("Email")
        presenter?.savePasswor()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullySaved, "Password is saved!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setRandomPassword()
        presenter?.savePasswor()
        XCTAssert((view as! MockViewCreatePassword).isSuccessfullySaved, "Password isn't saved!")
    }
    
    func testUpdateEmpty() {
        presenter?.setPasswordUpdating()
        presenter?.updatePassword()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullyUpdated, "Password is updated!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setPasswordName("Password name")
        presenter?.updatePassword()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullyUpdated, "Password is updated!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setPasswordEmailPin("Email")
        presenter?.updatePassword()
        XCTAssert(!(view as! MockViewCreatePassword).isSuccessfullyUpdated, "Password is updated!")
        XCTAssert((view as! MockViewCreatePassword).errorMessage == "You must fill all data!!!", "Error isn't shown!")
        
        presenter?.setRandomPassword()
        presenter?.updatePassword()
        XCTAssert((view as! MockViewCreatePassword).isSuccessfullyUpdated, "Password isn't updated!")
    }
    
    class MockCreateNewPasswordApi: CreateNewPasswordApi {
        
        required init(_ networkController: FBDBController) {
            
        }
        
        func savePassword(_ password: DTOPassword, userPasswords: [String], completition: @escaping (Bool) -> Void) {
            completition(true)
        }
        
        func update(_ password: DTOPassword, completition: @escaping (Bool) -> Void) {
            completition(true)
        }
        
    }
    
    class MockViewCreatePassword: ViewCreateNewPassword {
        
        var isSuccessfullySaved = false
        var isSuccessfullyUpdated = false
        var isAlertShown = false
        var isViewRefreshed = false
        var alertMessage = ""
        var errorMessage = ""
        
        func successfullySavedPassword() {
            isSuccessfullySaved = true
        }
        
        func successfullyUpdatePassword() {
            isSuccessfullyUpdated = true
        }
        
        func error(_ errorMessage: String) {
            self.errorMessage = errorMessage
        }
        
        func showAlert(_ alert: UIAlertController) {
            self.alertMessage = alert.title!
            isAlertShown = true
        }
        
        func refreshView() {
            isViewRefreshed = true
        }
        
    }
    
}
