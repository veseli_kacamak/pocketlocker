//
//  Pocket_lockerTests.swift
//  Pocket lockerTests
//
//  Created by Milan Bibeskovic on 12/6/16.
//  Copyright © 2016 App society. All rights reserved.
//

@testable import Pocket_locker
import XCTest

class LockerTestEmpty: XCTestCase {

    var presenter: AllPasswordsPresenter?
    var view: AllPasswordsView?
    var interactor: AllPasswordsInteractor?
    var api: AllPasswordsAPI?
    
    override func setUp() {
        super.setUp()
        api = MockAllPasswordAPI.init(FBDBController.sharedInstance)
        interactor = AllPasswordsInteractorImp.init(api!)
        view = MockAllPasswordsView()
        presenter = AllPasswordsPresenterImp.init(view!, interactor: interactor!, lockerListener: view as! LockerChangesListener)
    }

    override func tearDown() {
        api = nil
        view = nil
        interactor = nil
        presenter = nil
        super.tearDown()
    }

    func testEmptyMessage() {
        presenter?.getUserLocker()
        XCTAssert((view as! MockAllPasswordsView).isEmptyMessageShown, "empty screen isn't shown!")
    }
    
    class MockAllPasswordAPI: AllPasswordsAPI {
        
        required init(_ networkController: FBDBController) {
            
        }
        
        func getUserLocker(_ userId: String, completition: @escaping (Locker?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-lockers-empty")!
            let locker = Locker.init(fromDictionary: dic.value(forKey: "d97XbWpSRGP4tfTjTNnQC7MRL532") as! NSDictionary)
            completition(locker,nil)
        }
        
        func getPassword(_ passwordId: String, completition: @escaping (DTOPassword?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-passwords")!
            let passwords = DTOPassword.init(fromDictionary: dic.value(forKey: passwordId) as! NSDictionary)
            completition(passwords,nil)
        }
        
        func getCreditCard(_ creditCardId: String, completition: @escaping (DTOCard?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-cards")!
            let cards = DTOCard.init(fromDictionary: dic.value(forKey: creditCardId) as! NSDictionary)
            completition(cards,nil)
        }
        
        func deleteCreditCard(_ creditCardId: String, userCards: [String], completition: @escaping (Bool) -> Void) {
            completition(true)
        }
        
        func deletePassword(_ passwordId: String, userPasswords: [String], completition: @escaping (Bool) -> Void) {
            completition(true)
        }
    }
    
    class MockAllPasswordsView: AllPasswordsView, LockerChangesListener {
        
        var isAllPasswordsShown = false
        var isEmptyMessageShown = false
        var isEmptyMessageHidden = false
        var isPasswordsRefreshed = false
        var isCardsRefreshed = false
        var isUserLoggedOut = false
        var isSearchBarResseted = false
        var isLogoutDialogShown = false
        var isCreditCardSaved = false
        var isCreditCardUpdated = false
        var isCreditCardDeleted = false
        var isPasswordSaved = false
        var isPasswordUpdated = false
        var isPasswordDeleted = false
        var isOpeningPassword = false
        var isOpeningCreditCard = false
        var isEditingCreditCard = false
        var isEditingPassword = false
        var errorMessage = ""
        var alertDialogMessage = ""
        
        
        func showAllPasswordsAndCreditCards() {
            isAllPasswordsShown = true
        }
        
        func showError(_ errorMessage: String) {
            self.errorMessage = errorMessage
        }
        
        func showEmptyMessage() {
            isEmptyMessageShown = true
        }
        
        func hideEmptyMessage() {
            isEmptyMessageHidden = true
        }
        
        func refreshCreditCardsSection() {
            isCardsRefreshed = true
        }
        
        func refreshPasswordsSection() {
            isPasswordsRefreshed = true
        }
        
        func logoutUser(_ alert: UIAlertController) {
            isLogoutDialogShown = true
        }
        
        func successLogout() {
            isUserLoggedOut = true
        }
        
        func resetSearcBar() {
            isSearchBarResseted = true
        }
        
        func showDeleteAlert(_ alert: UIAlertController) {
            alertDialogMessage = alert.message!
        }
        
        func editCreditCard(_ creditCardId: String, selectedCreditCard: DTOCard, userCreditCardIds: [String]) {
            if !creditCardId.isEmpty &&
                !selectedCreditCard.pin.isEmpty &&
                !selectedCreditCard.name.isEmpty &&
                !userCreditCardIds.isEmpty {
                isEditingCreditCard = true
            }
        }
        
        func editPassword(_ passwordId: String, selectedPassword: DTOPassword, userPasswordIds: [String]) {
            if !passwordId.isEmpty &&
                !selectedPassword.eMail.isEmpty &&
                !selectedPassword.password.isEmpty &&
                !selectedPassword.name.isEmpty &&
                !userPasswordIds.isEmpty {
                isEditingPassword = true
            }
        }
        
        func newPassword() {
            isOpeningPassword = true
        }
        
        func newCreditCard() {
            isOpeningCreditCard = true
        }
        
        func savedCard() {
            isCreditCardSaved = true
        }
        
        func updatedCard() {
            isCreditCardUpdated = true
        }
        
        func deletedCard() {
            isCreditCardDeleted = true
        }
        
        func savedPassword() {
            isPasswordSaved = true
        }
        
        func updatedPassword() {
            isPasswordUpdated = true
        }
        
        func deletedPassword() {
            isPasswordDeleted = true
        }
    }

}
