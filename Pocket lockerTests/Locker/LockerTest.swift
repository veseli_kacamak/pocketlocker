//
//  LockerTest.swift
//  Pocket lockerTests
//
//  Created by Milan Bibeskovic on 3/12/18.
//  Copyright © 2018 App society. All rights reserved.
//

@testable import Pocket_locker
import XCTest

class LockerTest: XCTestCase {
    
    var presenter: AllPasswordsPresenter?
    var view: AllPasswordsView?
    var interactor: AllPasswordsInteractor?
    var api: AllPasswordsAPI?
    let PASSWORD_DELETE_MESSAGE = "Are you sure that you want to delete password?"
    let CREDIT_CARD_DELETE_MESSAGE = "Are you sure that you want to delete credit card?"
    
    
    override func setUp() {
        super.setUp()
        
        api = MockAllPasswordAPI.init(FBDBController.sharedInstance)
        interactor = AllPasswordsInteractorImp.init(api!)
        view = MockAllPasswordsView()
        presenter = AllPasswordsPresenterImp.init(view!, interactor: interactor!, lockerListener: view as! LockerChangesListener)
    }
    
    override func tearDown() {
        api = nil
        interactor = nil
        view = nil
        presenter = nil
        super.tearDown()
    }
    
    func testGetUserLocker() {
        presenter?.getUserLocker()
        XCTAssert(((self.view as! MockAllPasswordsView).isAllPasswordsShown),"There isn't any available lockers!")
        XCTAssert(((self.view as! MockAllPasswordsView).isCardsRefreshed),"There isn't any cards in the list!")
        XCTAssert(((self.view as! MockAllPasswordsView).isPasswordsRefreshed),"There isn't any passwords in the list!")
    }
    
    func testLogout(){
        presenter?.logoutUser()
        XCTAssert(((self.view as! MockAllPasswordsView).isLogoutDialogShown),"Login dialog is not showing!")
        presenter?.logoutAction()
        XCTAssert(((self.view as! MockAllPasswordsView).isUserLoggedOut),"User isn't logged out!")
    }
    
    func testDeleteCreditCard(){
        presenter?.deleteCreditCardAlert(!(presenter?.getCreditCards().isEmpty)! ? (presenter?.getCreditCards()[0].cardId)! : "1")
        XCTAssert(((self.view as! MockAllPasswordsView).alertDialogMessage == CREDIT_CARD_DELETE_MESSAGE),"Delete credit card alert isn't showed!")
        presenter?.deleteCreditCard(!(presenter?.getCreditCards().isEmpty)! ? (presenter?.getCreditCards()[0].cardId)! : "1")
        XCTAssert((self.view as! MockAllPasswordsView).isCreditCardDeleted,"Credit card isn't deleted!")
    }
    
    func testDeletePassword(){
        presenter?.deletePasswordAlert(!(presenter?.getLockerPasswords().isEmpty)! ? (presenter?.getLockerPasswords()[0].passwordId)! : "1")
        XCTAssert(((self.view as! MockAllPasswordsView).alertDialogMessage == PASSWORD_DELETE_MESSAGE),"Delete password alert isn't showed!")
        presenter?.deletePassword(!(presenter?.getLockerPasswords().isEmpty)! ? (presenter?.getLockerPasswords()[0].passwordId)! : "1")
        XCTAssert((self.view as! MockAllPasswordsView).isPasswordDeleted,"Password isn't deleted!")
    }
    
    func testEditCreditCard(){
        presenter?.getUserLocker()
        XCTAssert(((self.view as! MockAllPasswordsView).isAllPasswordsShown),"There isn't any available lockers!")
        XCTAssert(((self.view as! MockAllPasswordsView).isCardsRefreshed),"There isn't any cards in the list!")
        XCTAssert(((self.view as! MockAllPasswordsView).isPasswordsRefreshed),"There isn't any passwords in the list!")
        presenter?.openCreditCard(0)
        XCTAssert((self.view as! MockAllPasswordsView).isEditingCreditCard, "Error in editing credit card!")
    }
    
    func testEditPasswords(){
        presenter?.getUserLocker()
        XCTAssert(((self.view as! MockAllPasswordsView).isAllPasswordsShown),"There isn't any available lockers!")
        XCTAssert(((self.view as! MockAllPasswordsView).isCardsRefreshed),"There isn't any cards in the list!")
        XCTAssert(((self.view as! MockAllPasswordsView).isPasswordsRefreshed),"There isn't any passwords in the list!")
        presenter?.openPassword(0)
        XCTAssert((self.view as! MockAllPasswordsView).isEditingPassword, "Error in editing password!")
    }
    
    func testAddNewPassword(){
        presenter?.newPassword()
        XCTAssert((self.view as! MockAllPasswordsView).isOpeningPassword, "Error in adding new password!")
    }
    
    func testAddNewCreditCard(){
        presenter?.newCreditCard()
        XCTAssert((self.view as! MockAllPasswordsView).isOpeningCreditCard, "Error in adding new credit card!")
    }
    
    class MockAllPasswordAPI: AllPasswordsAPI {
        
        required init(_ networkController: FBDBController) {
            
        }
        
        func getUserLocker(_ userId: String, completition: @escaping (Locker?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-lockers")!
            let locker = Locker.init(fromDictionary: dic.value(forKey: "d97XbWpSRGP4tfTjTNnQC7MRL532") as! NSDictionary)
            completition(locker,nil)
        }
        
        func getPassword(_ passwordId: String, completition: @escaping (DTOPassword?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-passwords")!
            let passwords = DTOPassword.init(fromDictionary: dic.value(forKey: passwordId) as! NSDictionary)
            completition(passwords,nil)
        }
        
        func getCreditCard(_ creditCardId: String, completition: @escaping (DTOCard?, LockerError?) -> Void) {
            let dic = TestUtils.jsonFileToDictionary ("pocket-locker-cards")!
            let cards = DTOCard.init(fromDictionary: dic.value(forKey: creditCardId) as! NSDictionary)
            completition(cards,nil)
        }
        
        func deleteCreditCard(_ creditCardId: String, userCards: [String], completition: @escaping (Bool) -> Void) {
            completition(true)
        }
        
        func deletePassword(_ passwordId: String, userPasswords: [String], completition: @escaping (Bool) -> Void) {
            completition(true)
        }
    }
    
    class MockAllPasswordsView: AllPasswordsView, LockerChangesListener {
        
        var isAllPasswordsShown = false
        var isEmptyMessageShown = false
        var isEmptyMessageHidden = false
        var isPasswordsRefreshed = false
        var isCardsRefreshed = false
        var isUserLoggedOut = false
        var isSearchBarResseted = false
        var isLogoutDialogShown = false
        var isCreditCardSaved = false
        var isCreditCardUpdated = false
        var isCreditCardDeleted = false
        var isPasswordSaved = false
        var isPasswordUpdated = false
        var isPasswordDeleted = false
        var isOpeningPassword = false
        var isOpeningCreditCard = false
        var isEditingCreditCard = false
        var isEditingPassword = false
        var errorMessage = ""
        var alertDialogMessage = ""
        
        
        func showAllPasswordsAndCreditCards() {
            isAllPasswordsShown = true
        }
        
        func showError(_ errorMessage: String) {
            self.errorMessage = errorMessage
        }
        
        func showEmptyMessage() {
            isEmptyMessageShown = true
        }
        
        func hideEmptyMessage() {
            isEmptyMessageHidden = true
        }
        
        func refreshCreditCardsSection() {
            isCardsRefreshed = true
        }
        
        func refreshPasswordsSection() {
            isPasswordsRefreshed = true
        }
        
        func logoutUser(_ alert: UIAlertController) {
            isLogoutDialogShown = true
        }
        
        func successLogout() {
            isUserLoggedOut = true
        }
        
        func resetSearcBar() {
            isSearchBarResseted = true
        }
        
        func showDeleteAlert(_ alert: UIAlertController) {
            alertDialogMessage = alert.message!
        }
        
        func editCreditCard(_ creditCardId: String, selectedCreditCard: DTOCard, userCreditCardIds: [String]) {
            if !creditCardId.isEmpty &&
                !selectedCreditCard.pin.isEmpty &&
                !selectedCreditCard.name.isEmpty &&
                !userCreditCardIds.isEmpty {
                isEditingCreditCard = true
            }
        }
        
        func editPassword(_ passwordId: String, selectedPassword: DTOPassword, userPasswordIds: [String]) {
            if !passwordId.isEmpty &&
                !selectedPassword.eMail.isEmpty &&
                !selectedPassword.password.isEmpty &&
                !selectedPassword.name.isEmpty &&
                !userPasswordIds.isEmpty {
                isEditingPassword = true
            }
        }
        
        func newPassword() {
            isOpeningPassword = true
        }
        
        func newCreditCard() {
            isOpeningCreditCard = true
        }
        
        func savedCard() {
            isCreditCardSaved = true
        }
        
        func updatedCard() {
            isCreditCardUpdated = true
        }
        
        func deletedCard() {
            isCreditCardDeleted = true
        }
        
        func savedPassword() {
            isPasswordSaved = true
        }
        
        func updatedPassword() {
            isPasswordUpdated = true
        }
        
        func deletedPassword() {
            isPasswordDeleted = true
        }
    }
}
