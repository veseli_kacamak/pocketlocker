//
//  TestUtils.swift
//  Pocket lockerTests
//
//  Created by Milan Bibeskovic on 3/12/18.
//  Copyright © 2018 App society. All rights reserved.
//

import Foundation

open class TestUtils {
    
    class func jsonFileToDictionary (_ resourceFilePath :String) -> NSDictionary? {
        
        let bundle = Bundle(for: TestUtils.self)
        let path = bundle.path(forResource: resourceFilePath, ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        
        do {
            let JSON = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions(rawValue: 0))
            guard let JSONDictionary :NSDictionary = JSON as? NSDictionary else {
                return nil
            }
            return JSONDictionary
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        
        return nil
    }
}
